
# Khopyor

Приложение, ориентированное своей функциональностью на список: https://gitlab.com/apavlyut/msg-life-server/blob/master/Features.md

TODO:
master_id -> master_token? Но token не постоянный у пользователя.

Технологии:

  * Elixir/Phoenix
  * PostgreSQL

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
