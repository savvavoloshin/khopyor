
ExUnit.start(exclude: [:skip])
Faker.start()
Ecto.Adapters.SQL.Sandbox.mode(Khopyor.Repo, :manual)
{:ok, _} = Application.ensure_all_started(:ex_machina)
