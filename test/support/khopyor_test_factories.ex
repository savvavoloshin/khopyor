defmodule Khopyor.Factory do
  # with Ecto
  use ExMachina.Ecto, repo: Khopyor.Repo

  # without Ecto
  # use ExMachina

  def user_factory do
    %Khopyor.User{
      nickname: sequence(:nickname, &"Savva (#{&1})"),
      access_token: sequence(:access_token, &"1A2B3C4D5E_(#{&1})")
    }
  end

  def room_factory do
    %Khopyor.Room{
      title: sequence(:title, &"Example Room (#{&1})"),
    }
  end

  def message_factory do
    %Khopyor.Message{
      text: sequence(:text, &"sample text (#{&1})"),
    }
  end

end