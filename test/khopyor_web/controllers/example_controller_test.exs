defmodule KhopyorWeb.RoomControllerTest do
    use KhopyorWeb.ConnCase
    
    import Khopyor.Factory
    require Logger

    test "room_creation", %{conn: conn} do

      # предлагаю такой тест:
      # данный пользователь создаёт комнату
      # в комнате даны ещё два пользователя
      # данный пользователь создаёт ещё комнату
      # в ней дан ещё один пользователь - новый и
      # ещё один пользователь - первый из двух вышеуказанных

      # проверка правильности состоит в том, что
      # у данного пользователя в принадлежности - две комнаты
      # у остальных - по одной, кроме user_a, у которого - две

      # причём, данный пользователь может менять параметры комнаты,
      # а другие - не могут

      # дальше можно потестировать выбор общей комнаты

      user_master = insert(:user)
      user_a = insert(:user)
      user_b = insert(:user)
      user_c = insert(:user)


      primary_room_title = "Primary room"
      primary_room_description = "Primary room description"

      secondary_room_title = "Secondary room"
      secondary_room_description = "Secondary room description"

      # замечание: можно настроить создание комнат в ExMachina и
      # интересоваться созданием тысяч комнат

      user_ids_json_a = Jason.encode!([user_a.id, user_b.id])
      user_ids_json_b = Jason.encode!([user_c.id, user_a.id])

      params_a = [title: primary_room_title, description: primary_room_description, user_ids: user_ids_json_a]
      params_b = [title: secondary_room_title, description: secondary_room_description, user_ids: user_ids_json_b]

      conn = post(conn, "/api/users/#{user_master.id}/rooms", params_a)
      Logger.debug inspect(json_response(conn, 200))

      conn = post(conn, "/api/users/#{user_master.id}/rooms", params_b)
      Logger.debug inspect(json_response(conn, 200))

      assert length(Khopyor.Repo.all(Khopyor.Room)) == 2

      # проверка правильности:
      user = Khopyor.Repo.get_by(Khopyor.User, id: user_master.id)
      user = Khopyor.Repo.preload(user, [:rooms])

      user_a = Khopyor.Repo.get_by(Khopyor.User, id: user_a.id)
      user_a = Khopyor.Repo.preload(user_a, [:rooms])

      user_b = Khopyor.Repo.get_by(Khopyor.User, id: user_b.id)
      user_b = Khopyor.Repo.preload(user_b, [:rooms])

      user_c = Khopyor.Repo.get_by(Khopyor.User, id: user_c.id)
      user_c = Khopyor.Repo.preload(user_c, [:rooms])
      
      assert 2 == length(user.rooms)
      assert 2 == length(user_a.rooms)
      assert 1 == length(user_b.rooms)
      assert 1 == length(user_c.rooms)


      # это всё веселье на потом:
      
      # проверка общих комнат
      # ожидается, что у пользователей master и b будет одна общая комната,
      # а у master и a - две общие комнаты

      # запрос: выбрать различные комнаты, у которых в списках пользователей есть user_master и user_a
      # запрос: выбрать различные комнаты, у которых в списках пользователей есть user_master и user_b

      # rooms = from(room in Khopyor.Room, where: room.master_id == ^user_master.id) |> Khopyor.Repo.all

      # Logger.error inspect(rooms)
    end

    test "example", %{conn: conn} do
      # требуется проверка удаляемости ассоциаций
      # какие есть ассоциации?

      # 
    end
end