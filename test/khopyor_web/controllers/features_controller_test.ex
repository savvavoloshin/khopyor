defmodule KhopyorWeb.FeaturesControllerTest do
    use KhopyorWeb.ConnCase
    
    require Logger

    import Khopyor.Factory

    alias Khopyor.{Repo, User, Message}
    alias KhopyorWeb.Router.Helpers, as: Helpers

    @tag :skip
    test "1_authentication", %{conn: _conn} do
      # POST /users.json

      # пользователь указывает свой пароль
      # и если такой пользователь есть и
      # пароль совпадает то получает "access_token" ?

    end

    test "1_authentication_1_user_creation", %{conn: conn} do
      # POST /users.json

      # на данный момент для создания пользователя
      # требуем только позывной - nickname

      # тест состоит в создании тысячи пользователей
      # проверка правильности - в том, что в базе записана тысяча пользователей
      # нет, пусть будет десять тысяч, тогда уж

      # nicknames_count = 10000 
      nicknames_count = 100 # ладно уж, ждать ещё

      # Enum.each(0..x, fn i -> do_something(i) end)
      Enum.each(0..(nicknames_count-1), fn i ->
        nickname = "nickname_#{i}"
        params = [nickname: nickname]
        post(conn, "/api/users", params)
      end)

      # json_response(conn, 200)
      assert length(Repo.all(User)) == nicknames_count
    end

    test "1_authentication_2_user_logging_in", %{conn: conn} do
      # предполагая, что пользователь уже имеется,
      # авторизовать его по указанному имени, выдав access_token

      # правильность характерицуется тем, что в ответе пришёл access_token

      user = insert(:user)

      params = [nickname: user.nickname]

      request_path = Helpers.session_path(conn, :create)

      conn = post(conn, request_path, params)
      assert String.length(json_response(conn, 200)["access_token"]) == 4
    end
    
    @tag :skip
    test "1_1_enter_sms_code", %{conn: _conn} do
      # GET /users/:auth_token/check.json?auth_code=:auth_code
    end
    
    test "1_2_registration_update_details", %{conn: conn} do
      # PATCH /users/:auth_token.json

      # пользователь создан
      # известен его access_token

      # проверка правильности: поменялся nickname

      user = insert(:user)
      access_token = user.access_token
      new_nickname = "a_new_nickname"

      request_path = Helpers.user_path(conn, :update, access_token, nickname: new_nickname)
      conn = patch(conn, request_path)

      # Logger.debug inspect(json_response(conn, 200))
      assert json_response(conn, 200)["nickname"] == new_nickname
    end

    test "2_find_contact_by_nickname", %{conn: conn} do
      # GET /users/search.json?access_token=:access_token&nickname=:nickname

      # дан пользователь, дан позывной искомого пользователя
      # известно, что пользователь с искомым позывным существует
      # проверка правильности в том, что данные о нём получены

      user = insert(:user)
      user_a = insert(:user)

      access_token = user.access_token
      nickname = user_a.nickname

      request_path = Helpers.user_path(conn, :find_user, access_token, nickname: nickname)
      conn = get(conn, request_path)

      # Logger.debug inspect(json_response(conn, 200))
      assert json_response(conn, 200)["nickname"] == nickname
    end

    @tag :skip
    test "3_show_contacts_and_chats_of_a_user", %{conn: _conn} do
      # GET /users/:auth_token.json

      # рекомендуется обсуждение - что нужно? Чем отличаются контакты
      # от перечисленных в данных комнатах пользователей?

      # сделаю сейчас так: у данного пользователя
    end

    test "4_creating_room_1_private", %{conn: conn} do
      # ${host}/users/${user.access_token}/chats.json

      # создаётся "частная" комната
      # проверка правильности: стоит переключатель "частная"
      # данным пользователем это делается

      user = insert(:user)
     
      room_info = %{
        title: "title_sample",
        description: "sample_description",
        is_private: true
      }

      request_path = Helpers.user_room_path(conn, :create, user.access_token, room_info)
      conn = post(conn, request_path)

      Logger.debug inspect(json_response(conn, 200))

      assert length(json_response(conn, 200)["users"]) == 1
      assert json_response(conn, 200)["is_private"] == true

    end

    test "4_creating_room_2_group", %{conn: conn} do
      # ${host}/users/${user.access_token}/chats.json

      # создаётся "не частная" комната
      # проверка правильности: стоит переключатель "не частная"
      # данным пользователем это делается

      # помимо прочего, создаётся ещё несколько пользователей

      user = insert(:user)

      user_a = insert(:user)
      user_b = insert(:user)
      user_c = insert(:user)

      therir_access_tokens = [
        user_a.access_token,
        user_b.access_token,
        user_c.access_token
      ]
     
      room_info = %{
        title: "title_sample",
        description: "sample_description",
        is_private: false,
        access_tokens: therir_access_tokens
      }

      request_path = Helpers.user_room_path(conn, :create, user.access_token, room_info)
      
      conn = post(conn, request_path)

      Logger.debug inspect(json_response(conn, 200))

      assert length(json_response(conn, 200)["users"]) == 4
      assert json_response(conn, 200)["is_private"] == false

    end

    test "5_update_room_properties", %{conn: conn} do
      # PATCH /users/:access_token/chats/:chat_id.json

      # дан пользователь и у него есть комната, им же созданная
      # вот параметры комнаты и обновляются

      user = insert(:user)
      room = insert(:room, users: [user], master_id: user.id)
      
      access_token = user.access_token
      room_id = room.id
      params_to_change = [title: "12345"]
      request_path = Helpers.user_room_path(conn, :update, access_token, room_id, params_to_change: params_to_change)

      conn = patch(conn, request_path)

      Logger.debug inspect(json_response(conn, 200))
      assert json_response(conn, 200)["title"] == "12345"

    end

    test "6_sending_message_to_room", %{conn: conn} do
      # POST /users/:access_token/messages.json

      # есть комната с пользователем
      # пользователь отправляет сообщение в комнату
      # проверка правильности в том, что среди сообщений
      # комнаты появилось опубликованное пользователем сообщение

      user = insert(:user)
      room = insert(:room, users: [user], master_id: user.id)

      access_token = user.access_token
      room_id = room.id

      message_text = "Привет врачам!"

      message_params = %{message_text: message_text}

      request_path = Helpers.user_room_message_path(conn, :create, access_token, room_id, message_params)
      conn = post(conn, request_path)

      Logger.debug inspect(json_response(conn, 200))
      assert hd(json_response(conn, 200)["messages"])["text"] == message_text
    end

    test "7_removing_a_message_from_room", %{conn: conn} do
      # DELETE /users/:access_token/messages/:message_id.json

      # дана комната с пользователем
      # в которой опубликовано сообщение
      # пользователь принимает решение удалить сообщение
      # проверка правильности: сообщение удалено

      user = insert(:user)
      room = insert(:room, users: [user], master_id: user.id)

      message_a = insert(:message, user: user, room: room)
      insert(:message, user: user, room: room)

      access_token = user.access_token
      room_id = room.id
      message_id = message_a.id

      Logger.debug "! #{inspect message_a}"
      assert length(Repo.all(Message)) == 2

      request_path = Helpers.user_room_message_path(conn, :delete, access_token, room_id, message_id)
      delete(conn, request_path)

      assert length(Repo.all(Message)) == 1

    end

    @tag :skip
    test "8_mark_room_as_observed", %{conn: _conn} do
      # GET /users/:access_token/chats/:chat_id/read.json
    end

    @tag :skip
    test "9_push_to_ws_of_a_user", %{conn: _conn} do
      # User.notify
    end

    @tag :skip
    test "10_push_notification_on_a_users_device", %{conn: _conn} do
      # User.update_badge_counter Message.push_to_user
    end

    test "11_update_properties", %{conn: conn} do
      # PATCH /users/:auth_token/update_props.json

      # обновление параметров пользователя
      # проверка правильности: параметры обновились

      user = insert(:user)
      user_id = user.access_token
      # other_params = %{new_nickname: "new_nickname"}
      request_path = Helpers.user_path(conn, :update, user_id, nickname: "new_nickname")
      conn = patch(conn, request_path)

      Logger.debug inspect(json_response(conn, 200))
      assert json_response(conn, 200)["nickname"] == "new_nickname"
    end
end