defmodule Khopyor.Repo.Migrations.AddMasterIdToRoom do
  use Ecto.Migration

  def change do
  	alter table(:rooms) do      
      add :master_id, :integer
    end
  end
end
