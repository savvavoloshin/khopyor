defmodule Khopyor.Repo.Migrations.AddRoomsTable do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :title, :string
      add :description, :text

      timestamps()
    end
  end
end
