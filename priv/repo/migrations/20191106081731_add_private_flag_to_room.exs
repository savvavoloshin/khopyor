defmodule Khopyor.Repo.Migrations.AddPrivateFlagToRoom do
  use Ecto.Migration

  def change do
  	alter table(:rooms) do      
      add :is_private, :boolean, default: false
    end
  end
end
