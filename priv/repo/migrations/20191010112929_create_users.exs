defmodule Khopyor.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :nickname, :string
      add :password_digest, :string
      add :access_token, :string
      
      timestamps()
    end

  end
end
