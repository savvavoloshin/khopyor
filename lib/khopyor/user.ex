defmodule Khopyor.User do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:nickname, :password_digest, :access_token]}

  schema "users" do    
    field :nickname, :string
    field :password_digest, :string
    field :access_token, :string

    has_many :messages, Khopyor.Message
    many_to_many :rooms, Khopyor.Room, join_through: "rooms_users"

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:nickname, :access_token])
    |> validate_required([:nickname])
  end
end
