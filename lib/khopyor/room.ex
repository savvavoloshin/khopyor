defmodule Khopyor.Room do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:title, :description, :master_id, :users, :messages, :is_private]}

  schema "rooms" do

    field :title, :string
    field :description, :string
    field :master_id, :integer
    field :is_private, :boolean

    many_to_many :users, Khopyor.User, join_through: "rooms_users"
    
    has_many :messages, Khopyor.Message

    timestamps()

  end

  @doc false
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:title, :description, :is_private, :master_id])
    |> validate_required([:title, :master_id])
  end
end
