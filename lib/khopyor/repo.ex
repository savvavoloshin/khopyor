defmodule Khopyor.Repo do
  use Ecto.Repo,
    otp_app: :khopyor,
    adapter: Ecto.Adapters.Postgres
end
