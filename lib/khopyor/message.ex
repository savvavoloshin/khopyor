
defmodule Khopyor.Message do
    use Ecto.Schema
    import Ecto.Changeset

    @derive {Jason.Encoder, only: [:text]}
    
    schema "messages" do
      
      field :text, :string

      belongs_to :user, Khopyor.User

      belongs_to :room, Khopyor.Room

      timestamps()

    end

    def changeset(message, params) do
      message
      |> cast(params, [:text, :user_id, :room_id])
      |> validate_required([:user_id, :room_id])
    end
  end