defmodule KhopyorWeb.RoomController do
  use KhopyorWeb, :controller
  
  require Logger
  import Ecto.Query


  def create(conn, params) do
    
    # указанные штучки-дрючки можно посмотреть по адресу:
    # https://elixirschool.com/en/lessons/ecto/associations/#many-to-many

    # создаётся комната с данными параметрами

    Logger.debug "STAGE ZERO"

    Logger.debug "STAGE user"
    Logger.debug "inspect params #{inspect(params)}"
    user = Khopyor.Repo.get_by(Khopyor.User, access_token: params["user_id"])
    user_id = user.id

    Logger.debug "inspect user #{inspect(user)}"

    room_info = %{
      title: params["title"],
      description: params["description"],
      is_private: params["is_private"],
      master_id: user_id
    }

    Logger.debug "STAGE PLUS ZERO"
    Logger.debug "room_info #{inspect(room_info)}"

    room_changeset = Khopyor.Room.changeset(%Khopyor.Room{}, room_info)

    Logger.debug "STAGE MINUS ONE"

    Logger.debug "room_changeset #{inspect(room_changeset)}"
    
    Logger.debug "STAGE insert"
    
    {:ok, room} = Khopyor.Repo.insert(room_changeset)

    # затем ассоциируется с создавшим её пользователем
    # и с теми пользователями, которые указаны (даны)

    room = Khopyor.Repo.preload(room, [:users, :messages])
    user = Khopyor.Repo.get_by(Khopyor.User, access_token: params["user_id"])

    users = cond do
      is_nil(params["access_tokens"]) -> []
      !is_nil(params["access_tokens"]) ->
        user_access_tokens = (params["access_tokens"])
        from(u in Khopyor.User, where: u.access_token in ^user_access_tokens) |> Khopyor.Repo.all
    end

    users = [user | users]

    Logger.debug "STAGE associate_room_with_users"

    room = associate_room_with_users(room, users)

    json(conn, room)
  end

  def delete(conn, params) do
    Logger.debug "initiate deletion #{inspect(params)}"

    Logger.debug "STAGE user"
    Logger.debug "inspect params #{inspect(params)}"
    user = Khopyor.Repo.get_by(Khopyor.User, access_token: params["user_id"])
    user_id = user.id

    Logger.debug "inspect user #{inspect(user)}"

    room_info = %{
      title: params["title"],
      description: params["description"],
      is_private: params["is_private"],
      master_id: user_id
    }

    room = Khopyor.Repo.get_by(Khopyor.Room, master_id: user_id, title: params["title"])

    {:ok, room} = room |> Khopyor.Repo.delete()
    room = Khopyor.Repo.preload(room, [:users, :messages])

    json(conn, room)

  end

  def update(conn, params) do
    Logger.debug "!!!!!!!!!!!!!!"
    Logger.debug inspect(params)
    Logger.debug inspect(params["params_to_change"])
    params_to_change = %{title: params["params_to_change"]["title"]}
    Logger.debug inspect(params_to_change)

    room = Khopyor.Repo.get_by(Khopyor.Room, id: params["id"])
    {:ok, room} = Ecto.Changeset.change( room, params_to_change) |> Khopyor.Repo.update()
    room = Khopyor.Repo.preload(room, [:users, :messages])
    json(conn, room)
  end

  defp associate_room_with_users(room, users) do
    Logger.debug inspect("associate_room_with_users")
    Logger.debug inspect(room)
    Logger.debug inspect(users)
    room_changeset = Ecto.Changeset.change(room)
    room_users_changeset = room_changeset |> Ecto.Changeset.put_assoc(:users, users)
    Khopyor.Repo.update!(room_users_changeset)
  end

end