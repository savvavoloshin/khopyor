defmodule KhopyorWeb.SessionController do
    use KhopyorWeb, :controller
    require Logger
    
    def create(conn, params) do
        Logger.debug "creating session"
        
        nickname = params["nickname"]

        user = Khopyor.Repo.get_by(Khopyor.User, nickname: nickname)

        if user do
            Logger.debug "with user"
            access_token = generate_access_token()
            {:ok, user} = set_access_token(user, access_token)

            Logger.debug inspect(user)
            
            json(conn, user)
        else
            Logger.debug "without user"
            json(conn, %{status: 403})
        end
    end

    def delete(_conn, _params) do
    end
    
    def generate_password_hash(_password) do
    end
    
    def generate_access_token() do
        length = 4
        access_token = :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
        access_token
    end

    def set_access_token(user, access_token) do
        Ecto.Changeset.change( user, %{access_token: access_token}) |> Khopyor.Repo.update()
    end

    def reset_access_token(user) do
        Ecto.Changeset.change( user, %{access_token: nil}) |> Khopyor.Repo.update()
    end

  end