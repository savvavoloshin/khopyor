defmodule KhopyorWeb.PageController do
  use KhopyorWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
