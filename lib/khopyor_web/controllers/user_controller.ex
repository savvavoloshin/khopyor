defmodule KhopyorWeb.UserController do
  use KhopyorWeb, :controller
  
  require Logger

  def some_random_string() do
    :crypto.strong_rand_bytes(16) |> Base.url_encode64 |> binary_part(0, 16)
  end

  def choose_state_name(state_index) do
    case state_index do
      0 -> :just_created
      1 -> :details_filled
      2 -> :avatar_uploaded
      3 -> :removed
    end
  end

  def gen_auth_code() do
    # пусть код будет трёхзначный
    a = :rand.uniform(9)+1
    b = :rand.uniform(10)
    c = :rand.uniform(10)
    code = a*100+b*10+c
    code
  end

  def create(conn, params) do

    Logger.debug "create PARAMS#{inspect(params)}"
    
    access_token = KhopyorWeb.SessionController.generate_access_token()

    user_info = %{ nickname: params["nickname"], access_token: access_token }
    Logger.debug "create PARAMS#{inspect(user_info)}"
    changeset = Khopyor.User.changeset(%Khopyor.User{}, user_info)
    {:ok, user} = Khopyor.Repo.insert(changeset)
    json(conn, user)
  end

  def delete(conn, params) do
    Logger.debug "delete PARAMS#{inspect(params)}"
    user = Khopyor.Repo.get_by(Khopyor.User, access_token: params["id"])
    Logger.debug "user PARAMS#{inspect(user)}"
    user |> Khopyor.Repo.delete()
    json(conn, :ok)
  end

  def update(conn, params) do
    Logger.debug "update PARAMS#{inspect(params)}"
    user = Khopyor.Repo.get_by(Khopyor.User, access_token: params["id"])
    Logger.debug inspect(user)
    {:ok, user} = Ecto.Changeset.change( user, %{nickname: params["nickname"]}) |> Khopyor.Repo.update()
    json(conn, user)
  end

  def find_user(conn, params) do
    _user = Khopyor.Repo.get_by(Khopyor.User, access_token: params["user_id"])

    found_user = Khopyor.Repo.get_by(Khopyor.User, nickname: params["nickname"])

    json(conn, found_user)
  end

end