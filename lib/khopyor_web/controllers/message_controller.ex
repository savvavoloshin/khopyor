defmodule KhopyorWeb.MessageController do
  use KhopyorWeb, :controller

  require Logger

  def get(_conn, _params) do
  end

  def create(conn, params) do
    
    message_text = params["message_text"]
    room_id = params["room_id"]

    room = Khopyor.Repo.get_by(Khopyor.Room, id: room_id)

    message = Ecto.build_assoc(room, :messages, %{text: message_text})
    message = Khopyor.Repo.insert!(message)

    Logger.debug "MESSAGE is text! #{ message_text}"
    Logger.debug "MESSAGE is inserted! #{ inspect(message)}"

    room = Khopyor.Repo.preload(room, [:messages, :users])

    Logger.debug "response is #{ inspect(room)}"
    Logger.debug "broadcasting..."
    KhopyorWeb.Endpoint.broadcast!("room:lobby", "new:msg", %{a: "b"})
      
    json(conn, room)
    
  end

  def delete(conn, params) do
    Logger.debug "params #{inspect params}"
    Logger.debug "message_id #{inspect params["id"]}"
    message = Khopyor.Repo.get_by(Khopyor.Message, id: params["id"])
    Logger.debug "message #{inspect message}"
    Khopyor.Repo.delete(message)
    json(conn, %{response: :message_is_destroyed})
  end

  def update(_conn, _params) do
  end

end