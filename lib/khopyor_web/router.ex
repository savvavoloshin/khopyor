defmodule KhopyorWeb.Router do
  use KhopyorWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", KhopyorWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", KhopyorWeb do
    pipe_through :api

    resources "/sessions", SessionController, only: [:create]

    resources "/users", UserController, only: [:update, :delete, :create]

    get "/users/:user_id/find_user", UserController, :find_user

    resources "/users", UserController do
      resources "/rooms", RoomController do
        resources "/messages", MessageController
      end
    end

  end
end
