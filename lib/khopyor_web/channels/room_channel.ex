defmodule KhopyorWeb.RoomChannel do
    use Phoenix.Channel

    require Logger

    Logger.debug "KhopyorWeb.RoomChannel"
  
    def join("room:lobby", _message, socket) do
      {:ok, socket}
    end

    def join("room:" <> _room_id, _payload, socket) do
      {:ok, socket}
    end

    def handle_in("new_msg", %{"body" => body}, socket) do
      # broadcast!(socket, "new_msg", %{body: body})
      broadcast!(socket, "new_msg", %{body: body})
      {:reply, %{"body" => body}, socket}
    end
    
    def handle_in("new:msg", message, socket) do
      Logger.debug "handle_in"
      # {:reply, {:ok, message}, socket}
      
      broadcast!(socket, "new:msg", %{what_to_say: "this1"})
      broadcast!(socket, "new:msg", %{what_to_say: "this2"})
      broadcast!(socket, "new:msg", %{what_to_say: "this3"})

      {:reply, {:ok, %{computer: message}}, socket}
    end
  
end