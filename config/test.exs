use Mix.Config

# Configure your database
config :khopyor, Khopyor.Repo,
  username: "savva",
  password: "181483331264",
  database: "khopyor_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :khopyor, KhopyorWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print not only warnings and errors during test
# config :logger, level: :debug
config :logger, level: :warn
